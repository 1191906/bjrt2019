package aplicação;


import java.io.*;
import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import static aplicação.Menu.VerificaUtilizadores;

public class Jogo2 {

    private static String nome, animal, objeto, marca, prof, N, A, O, M, P;
    private static final Scanner scan = new Scanner(System.in);
    private static int i, add, pontos;
    private static char SN;
    private static String[] nome1, animal1, objeto1, marca1, prof1 = new String[5000];
    private static final String path = Paths.get("").toAbsolutePath().toString();
    private static int jogadas,totalp = 0;
    private static int totalvj = 1;
    private static int[] dados;



    public static int correrJogo2() {
        // variaveis 
        char escolha, ch;
        boolean input = true;

        // pequeno menu de jogo
        System.out.println("Bem vindo ao jogo do STOP\n");
        System.out.println("Prima (1) para iniciar o jogo");
        System.out.println("Prima (0) para sair para o menu");
        escolha = scan.next().charAt(0);

        switch (escolha) {
            case '0':
                //System.out.println("See you later");
                
                break;
            case '1':
                totalp=0;
                Stop();

            default:
                System.out.println("A sua escolha deve ser entre 0 e 1 ");
                break;
        }
        return totalp;
    }                      //pequeno menu do jogo
    
    public static int score(){
        
        totalp = totalp + pontos;
   
    return totalp;
    }
    // vezes jogadas
    public static int vezesjogadas(){ 
        
        totalvj = totalvj + 1;
   
    return totalvj;
    }
    public static boolean retornajogarnovamente() {

        char SN;
        boolean TF = true; // TF = true or false
        

            System.out.println("Deseja continuar a jogar? S(Sim) / N(Não)");
            SN = scan.next().charAt(0);
            
            if (SN == 'S' || SN == 's') {
                System.out.println("No jogo "+(jogadas + 1)+" fez "+pontos+" pontos.\n");
                jogadas = (jogadas +1);
                vezesjogadas();
                pontos = 0;
                
            } else if (SN == 'N' || SN == 'n') {
                System.out.println("Jogou: "+(jogadas + 1)+" vez(es) e a sua última pontuação é de: "+pontos+" pontos!\n");
                //System.out.println(totalp); // teste pontuação final
                //System.out.println(totalvj); // teste jogadas
                TF = false;
            }
        
    return TF;
    }

    // leitores de ficheiros 
    public static String[] lerDicNome() {

        String[] dicNome = new String[1000];
        int add = 0;

        try {
            List<String> allLines = Files.readAllLines(Paths.get(path + "\\Nome.txt"));
            for (String line : allLines) {

                dicNome[add++] = line;
            }
        } catch (IOException e) {
            System.out.println("O caminho não está correto!");
        }

        return dicNome;
    }
    public static String[] lerDicAnimal() {

        String[] dicAnimal = new String[1000];
        int add = 0;

        try {
            List<String> allLines = Files.readAllLines(Paths.get(path + "\\Animal.txt"));
            for (String line : allLines) {

                dicAnimal[add++] = line;
            }
        } catch (IOException e) {
            System.out.println("O caminho não está correto!");
        }

        return dicAnimal;
    }
    public static String[] lerDicObjeto() {

        String[] dicObjeto = new String[1000];
        int add = 0;

        try {
            List<String> allLines = Files.readAllLines(Paths.get(path + "\\Objeto.txt"));
            for (String line : allLines) {

                dicObjeto[add++] = line;
            }
        } catch (IOException e) {
            System.out.println("O caminho não está correto!");
        }

        return dicObjeto;
    }
    public static String[] lerDicMarca() {

        String[] dicMarca = new String[1000];
        int add = 0;

        try {
            List<String> allLines = Files.readAllLines(Paths.get(path + "\\Marca.txt"));
            for (String line : allLines) {

                dicMarca[add++] = line;
            }
        } catch (IOException e) {
            System.out.println("O caminho não está correto!");
        }

        return dicMarca;
    }
    public static String[] lerDicProf() {

        String[] dicProf = new String[1000];
        int add = 0;

        try {
            List<String> allLines = Files.readAllLines(Paths.get(path + "\\Prof.txt"));
            for (String line : allLines) {

                dicProf[add++] = line;
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!");
        }

        return dicProf;
    }
    
    // Escrever no ficheiro
    public static void escreverNome() {

        try (FileWriter fw = new FileWriter(path + "\\Nome.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
             out.println(nome);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!");
        }
    }
    public static void escreverAnimal() {

        try (FileWriter fw = new FileWriter(path + "\\Animal.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
             out.println(animal);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!");
        }
    }
    public static void escreverObjeto() {

        try (FileWriter fw = new FileWriter(path + "\\Objeto.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
                out.println(objeto);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!)");
        }
    }
    public static void escreverMarca() {

        try (FileWriter fw = new FileWriter(path + "\\Marca.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
                out.println(marca);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!");
        }
    }
    public static void escreverProf() {

        try (FileWriter fw = new FileWriter(path + "\\Prof.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
                out.println(prof);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!");
        }
    }
    public static void escreverScore(){
        
           try (FileWriter fw = new FileWriter(path + "\\score.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
                out.println(totalp);


        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!)");
        }
    
    
    }

    public static String bfr() {
        //Enter data using BufferReader 
        BufferedReader ler = new BufferedReader(new InputStreamReader(System.in));
//        System.out.println("Nova maneira de ler");

        // Reading data using readLine 
        String input = "";
        try {
            input = ler.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Jogo2.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Printing the read line 

//        System.out.println("resultado = " + input); 
        return input;
    }                                  // alternativa ao scan.next() //permite ler o enter como input

    public static int Stop() {
    
        //variaveis
        pontos = 0;
        i = 0;
        nome1 = lerDicNome();
        animal1 = lerDicAnimal();
        objeto1 = lerDicObjeto();
        marca1 = lerDicMarca();
        prof1 = lerDicProf();
        // comprimento  da string
        int lgn = nome1.length;
        int lga = animal1.length;
        int lgo = objeto1.length;
        int lgm = marca1.length;
        int lgp = prof1.length;
        
        boolean g = true;
        
    while(g ==true){
        
        Random r = new Random();                                                // random letra
        String c = String.valueOf((char) (r.nextInt(26) + 'A'));
        System.out.println("\nA sua letra é : " + c);

        System.out.println("Nome");                                             //nome ----------------------------------------------------
        nome = bfr();

        if (nome.length() > 0) {

            N = nome.substring(0, 1).toUpperCase();                                  // obter a letra pela qual a palavra começa

            if (N.equals(c)) {                                                      // verificação da letra inicial
                for (i = 0; i < lgn; i++) {
                    if (nome.equals(nome1[i])) {                                       //verificação da palavra no dicionário
                        System.out.println("Acertou posição\n");
                        pontos =(pontos+20);
                        break;
                    } else if (!nome.equals(nome1[i]) && i >= lgn -1) {                  //caso a letra inicial esteja correta mas a palavra não exista no dicionário
                        while (true) {
                            System.out.println("Palavra não encontrada, deseja adicionar selecione S(Sim) / N(Não) ");
                            SN = scan.next().charAt(0);
                            if (SN == 'S' || SN == 's') {
                                escreverNome();                                              // adicionar palavra
                                System.out.println("Palavra adicionada com sucesso");
                                pontos =(pontos+10);
                                break;
                            } else if (SN == 'N' || SN == 'n') {
                                System.out.println("Palavra não adicionada!");
                                break;
                            }
                        }
                    }
                }
            } else {
                System.out.println("0 pontos");
            }
        } else {
            System.out.println("0 pontos");
        }

        System.out.println("Animal");                                           //animal ---------------------------------------------------
        animal = bfr();

        if (animal.length() > 0) {
            
            A = animal.substring(0, 1).toUpperCase();                               // obter a letra pela qual a palavra começa 
                    
            if (A.equals(c)) {                                                      // verificação da letra inicial
                for (i = 0; i < lgn; i++) {
                    if (animal.equals(animal1[i])) {                                       //verificação da palavra no dicionário
                        System.out.println("Acertou posição\n");
                        pontos =(pontos+20);
                        break;
                    } else if (!animal.equals(animal1[i]) && i >= lgn - 1) {                  //caso a letra inicial esteja correta mas a palavra não exista no dicionário
                        while (true) {
                            System.out.println("Palavra não encontrada, deseja adicionar selecione S(Sim) / N(Não) ");
                            SN = scan.next().charAt(0);
                            if (SN == 'S' || SN == 's') {
                                escreverAnimal();                                              // adicionar palavra
                                System.out.println("Palavra adicionada com sucesso");
                                pontos =(pontos+10);
                                break;
                            } else if (SN == 'N' || SN == 'n') {
                                System.out.println("Palavra não adicionada!");
                                break;
                            }
                        }
                    }
                }
            } else {
                System.out.println("0 pontos");
            }
        } else {
            System.out.println("0 pontos");
        }

        System.out.println("Objeto");                                           //objeto --------------------------------------------------------
        objeto = bfr();
        
        if (objeto.length() > 0) {
            
            O = objeto.substring(0, 1).toUpperCase();                               // obter a letra pela qual a palavra começa 
                    
            if (O.equals(c)) {                                                      // verificação da letra inicial
                for (i = 0; i < lgn; i++) {
                    if (objeto.equals(objeto1[i])) {                                       //verificação da palavra no dicionário
                        System.out.println("Acertou posição\n");
                        pontos =(pontos+20);
                        break;
                    } else if (!objeto.equals(objeto1[i]) && i >= lgn - 1) {                  //caso a letra inicial esteja correta mas a palavra não exista no dicionário
                        while (true) {
                            System.out.println("Palavra não encontrada, deseja adicionar selecione S(Sim) / N(Não) ");
                            SN = scan.next().charAt(0);
                            if (SN == 'S' || SN == 's') {
                                escreverObjeto();                                              // adicionar palavra
                                System.out.println("Palavra adicionada com sucesso");
                                pontos =(pontos+10);
                                break;
                            } else if (SN == 'N' || SN == 'n') {
                                System.out.println("Palavra não adicionada!");
                                break;
                            }
                        }
                    }
                }
            } else {
                System.out.println("0 pontos");
            }
        } else {
            System.out.println("0 pontos");
        }

    

        System.out.println("Marca");                                            //marca ---------------------------------------------------------------
        marca = bfr();

        if (marca.length() > 0) {
            
            M = marca.substring(0, 1).toUpperCase();                               // obter a letra pela qual a palavra começa 
                    
            if (M.equals(c)) {                                                      // verificação da letra inicial
                for (i = 0; i < lgn; i++) {
                    if (marca.equals(marca1[i])) {                                       //verificação da palavra no dicionário
                        System.out.println("Acertou posição\n");
                        pontos =(pontos+20);
                        break;
                    } else if (!marca.equals(marca1[i]) && i >= lgn - 1) {                  //caso a letra inicial esteja correta mas a palavra não exista no dicionário
                        while (true) {
                            System.out.println("Palavra não encontrada, deseja adicionar selecione S(Sim) / N(Não) ");
                            SN = scan.next().charAt(0);
                            if (SN == 'S' || SN == 's') {
                                escreverMarca();                                              // adicionar palavra
                                System.out.println("Palavra adicionada com sucesso");
                                pontos =(pontos+10);
                                break;
                            } else if (SN == 'N' || SN == 'n') {
                                System.out.println("Palavra não adicionada!");
                                break;
                            }
                        }
                    }
                }
            } else {
                System.out.println("0 pontos");
            }
        } else {
            System.out.println("0 pontos");
        }

        System.out.println("Profissão");                                        //profissão -----------------------------------------------------------------
        prof = bfr();
        
        if (prof.length() > 0) {
            
            P = prof.substring(0, 1).toUpperCase();                               // obter a letra pela qual a palavra começa 
                    
            if (P.equals(c)) {                                                      // verificação da letra inicial
                for (i = 0; i < lgn; i++) {
                    if (prof.equals(prof1[i])) {                                       //verificação da palavra no dicionário
                        System.out.println("Acertou posição\n");
                        pontos =(pontos+20);
                        break;
                    } else if (!prof.equals(prof1[i]) && i >= lgn - 1) {                  //caso a letra inicial esteja correta mas a palavra não exista no dicionário
                        while (true) {
                            System.out.println("Palavra não encontrada, deseja adicionar selecione S(Sim) / N(Não) ");
                            SN = scan.next().charAt(0);
                            if (SN == 'S' || SN == 's') {
                                escreverProf();                                              // adicionar palavra
                                System.out.println("Palavra adicionada com sucesso");
                                pontos =(pontos+10);
                                break;
                            } else if (SN == 'N' || SN == 'n') {
                                System.out.println("Palavra não adicionada!");
                                break;
                            }
                        }
                    }
                }
            } else {
                System.out.println("0 pontos");
            }
        } else {
            System.out.println("0 pontos");
        }
        score();
        
        g = retornajogarnovamente();
    }
    return totalp;
    }

}