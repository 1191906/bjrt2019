package aplicação;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.FilterInputStream;
import java.io.IOException;
import java.util.Scanner;
import static jdk.nashorn.tools.ShellFunctions.input;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Jogo1 
{
  private static Object sc;
    /**
     * @param args the command line arguments
     */

//Carrega perguntas de acordo com o nivel dificuldade do ficheiro local TXT
public static String[] perguntasfaceis(){ 
    String[] peasy = new String[100];
    int ad=0;
    String path = Paths.get("").toAbsolutePath().toString();
    try {
            List<String> allLines = Files.readAllLines(Paths.get(path+"\\easyquestions.txt"));
            for (String line : allLines) 
            {
                peasy[ad++]= line;
            }
		} 
        catch (IOException e) 
        {
            e.printStackTrace();
	}
   return peasy;
  }

//Carrega perguntas de acordo com o nivel dificuldade do ficheiro local TXT
public static String[] perguntasmed(){ 
    String[] pmed = new String[100];
    int ad=0;
    String path1 = Paths.get("").toAbsolutePath().toString();
    try {
            List<String> allLines = Files.readAllLines(Paths.get(path1+"\\mediumquestions.txt"));
            for (String line : allLines) 
            {
                pmed[ad++]= line;
            }
		} 
        catch (IOException e) 
        {
            e.printStackTrace();
	}
   return pmed;
  }

//Carrega perguntas de acordo com o nivel dificuldade do ficheiro local TXT
public static String[] perguntasdif(){ 
    String[] phard = new String[100];
    int ad=0;
    String path2 = Paths.get("").toAbsolutePath().toString();
    try {
            List<String> allLines = Files.readAllLines(Paths.get(path2+"\\hardquestions.txt"));
            for (String line : allLines) 
            {
                phard[ad++]= line;
            }
		} 
        catch (IOException e) 
        {
            e.printStackTrace();
	}
   return phard;
  }

// gera possibilidades de respostas 50 50
public static int[] gerar(int rcerta){ 
    int[] biblio;
    biblio = new int[20];
    Random rand = new Random();
    int i=0;
    
    while(i!=20)
    {
        int gerar=rand.nextInt(5);
         if(i!=0 && gerar!=rcerta && gerar!=0 && gerar!=biblio[i-1] && gerar<5)
         {
            biblio[i]=gerar;
            i++;
         }
         
         if(i==0 && gerar!=rcerta && gerar!=0 && gerar<5)
         {
        biblio[0]=gerar;
        i++;
         }
     }
		
   return biblio;
  }

//escrever 50 50
public static String[] ElimCinq(int E1, int E2){ 
String[] prot = new String[2];
switch(E1) 
    { 
     case 1: 
        prot[0]="a";
        break;
     case 2: 
        prot[0]="b";
        break;
     case 3: 
        prot[0]="c";
        break;
     case 4: 
        prot[0]="d";
        break;
    }
switch(E2) 
    { 
     case 1: 
        prot[1]="a";
        break;
     case 2: 
        prot[1]="b";
        break;
     case 3: 
        prot[1]="c";
        break;
     case 4: 
        prot[1]="d";
        break;
    }
   return prot;
  }

//menu do jogo 1
public static int[] inicio(){
Scanner ler1 = new Scanner(System.in);
int[] dados;
int armzPont=0,Nvezes=1;
String fim;
dados = new int[5];

System.out.println("\nBem vindo ao Jogo QuemQuerSerMilionário ");
System.out.println("\nSelecione a opcao correcta de A a D :");
System.out.println("Ajudas disponiveis por jogo: 50%50 e troca de pergunta");
System.out.println("Prima [S] para Desistir");
dados=ApresPerguntas(0);
armzPont=armzPont+dados[1];
do{
System.out.println("\nDeseja Jogar novamente? ");
System.out.println("1 Iniciar Novamente e 0 Sair");
fim=ler1.next();
if("1".equals(fim))
 {
    Nvezes++;
    System.out.println("\nDeseja Jogar novamente? ");
    dados=ApresPerguntas(1);
    armzPont=armzPont+dados[1];
 }
}
while(!"0".equals(fim));
dados[1]=armzPont;
dados[0]=Nvezes;
return dados;
  }

//apresentacao de perguntas ao utilizador
public static int[] ApresPerguntas(int reset){
Scanner ler = new Scanner(System.in);
Scanner ler1 = new Scanner(System.in);
int pont=0,nivel=0,cinquenta=1,troca=1,posic=0,ctt=0,valor=0;
String[] ns = new String[100];
String[] Elim = new String[5];
String sf,sf1,conf,ajudasW = null,ajudasQ = null;
int[] pc,dados;
pc = new int[5];
dados = new int[5];
dados[0]=1;
Elim[0]="";
Elim[1]="";

if(reset==1)
{
posic=0;
valor=1;
ctt=0;
nivel=0;
troca=1;
cinquenta=1;
ns=perguntasfaceis();
}
        while(nivel<15){
             switch(nivel) 
        { 
            case 0: 
                ns=perguntasfaceis();
                System.out.println("\nInicio Perguntas Faceis nivel 1\n");
                valor=1;
                break; 
            case 5: 
                posic = 0;
                ctt=0;
                ns=perguntasmed();
                System.out.println("\nInicio Perguntas Medias nivel 2\n");
                valor=2;
                break; 
            case 10: 
                posic = 0;
                ctt=0;
                ns=perguntasdif();
                System.out.println("\nInicio Perguntas Dificeis nivel 3\n");
                valor=5;
                break;
        } 
  
    do   {              System.out.println("Pergunta nº "+(nivel+1)+", com valor de "+valor+" ponto(s) cada");
                        System.out.println(ns[posic]);
                        System.out.println("[A] "+ns[posic+1]+"\t\t[B] "+ns[posic+2]);
                        System.out.println("[C] "+ns[posic+3]+"\t\t[D] "+ns[posic+4]);
                        if(troca==1)
                        {
                        ajudasW="  [W] Troca de pergunta";
                        }
                        if(troca==0)
                        {
                            ajudasW="";
                        }
                        if(cinquenta==1)
                        {
                        ajudasQ="  [Q] 50:50 Elimina";
                        }
                        if(cinquenta==0)
                        {
                            ajudasQ="";
                        }
                       
                        if(cinquenta==1 || troca==1)
                        {
                        System.out.println("Ajudas disponiveis:"+ajudasQ+""+ajudasW);
                        }
                            sf1 = ler.next();  
                            sf = sf1.toLowerCase(); 
                        
                         if(("w".equals(sf) ||"W".equals(sf)) &&  troca==1)
                         {
                            posic=posic+6;
                            troca--;
                            System.out.println(" ");
                         }
                        
                        if(("q".equals(sf) ||"Q".equals(sf)) &&  cinquenta==1 )
                        { 
                            
                            conf=ns[posic+5];
                     
                            switch(conf) {
                                case "a":
                                    pc=gerar(1);
                                    for(int i=0;i<2;i++)
                                    {
                                        ns[posic+pc[i]]="-----";
                                        ns[posic+pc[i]]="-----";
                                    }
                                    Elim=ElimCinq(pc[0],pc[1]);
                                   break; 
                                case "b":
                                    pc=gerar(2);
                                    for(int i=0;i<2;i++)
                                    {
                                        ns[posic+pc[i]]="-----";
                                        ns[posic+pc[i]]="-----";
                                    }
                                    Elim=ElimCinq(pc[0],pc[1]);
                                    break; 
                                case "c":
                                    pc=gerar(3);
                                    for(int i=0;i<2;i++)
                                    {
                                        ns[posic+pc[i]]="-----";
                                        ns[posic+pc[i]]="-----";
                                    }
                                    Elim=ElimCinq(pc[0],pc[1]);
                                    break; 
                                case "d":
                                    pc=gerar(4);
                                    for(int i=0;i<2;i++)
                                    {
                                        ns[posic+pc[i]]="-----";
                                        ns[posic+pc[i]]="-----";
                                    }
                                    Elim=ElimCinq(pc[0],pc[1]);
                                    break; 
                                    
                            }
                            cinquenta--;
                            System.out.println(" ");
                        
                        }
                        if(sf.equals("s"))
                        {
                            nivel=20;
                        }
                        if(sf.equals(ns[posic+5]))
                        {
                            System.out.println("\tACERTOU "+(nivel+1));
                            System.out.println(" ");
                            nivel++;
                            ctt++;
                            posic=posic+6;
                            pont=pont+valor;
                            Elim[0]="";
                            Elim[1]="";

                        }else if(!"w".equals(sf) && !"q".equals(sf) && !Elim[0].equals(sf)&& !Elim[1].equals(sf))
                        { 
                            if(sf.equals("a")|| sf.equals("b")|| sf.equals("c")|| sf.equals("d"))
                            {
                            System.out.println("\t----ERROU----");
                            nivel=20;
                            }
                        }
 
        }  while(ctt!=5 && nivel<9 && !"s".equals(sf));
            
    
     }
        System.out.println("\tA sua Pontuacao é: "+pont);
        dados[1]=pont;
	return dados;	
} 

//PRINCIPAL
public static int correrJogo1()
    {
int[] a;
a = new int[5];
a=inicio();
System.out.println("\tJogou "+a[0]+" vezes\n"+"\tAcumulou "+a[1]+" pontos");
    return a[1];
    }
}