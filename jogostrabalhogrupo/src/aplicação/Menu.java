package aplicação;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Menu {
private static String[] DadosPont = new String[100];
private static int PosicaoJogador;
    
public static void menu(){
        System.out.println("\t  MENU TRABALHO DE GRUPO BJRT2019");
        System.out.println("\t===================================");
        System.out.println("\t| 1. Jogo Quem Quer Ser Milionário |");
        System.out.println("\t| 2. Jogo STOP                     |");
        System.out.println("\t| 0. Sair                          |");
        System.out.println("\t===================================");
        System.out.println("\tOpção:");
    }

public static void Jogo1() throws IOException{
    int pont=0,pontAnt;
        System.out.println("Entrou no Jogo 1\n");
        // instanciar classe Jogo1
        Jogo1 variavel = new Jogo1();
        
        // chamada de método correrJogo
        pontAnt=Integer.parseInt(DadosPont[PosicaoJogador]);
        pont=variavel.correrJogo1();
        System.out.println("PONTOS TOTAIS\n"+pont);
        DadosPont[PosicaoJogador]=String.valueOf(pont+pontAnt);
        Limpeza();
        SavePontuacaoN();
    }

public static void Jogo2() throws IOException{
    int pont=0,pontAnt;
        System.out.println("Entrou no Jogo 2\n");
        // instanciar classe Jogo1
        Jogo2 variavel = new Jogo2();
        
        // chamada de método correrJogo
        pontAnt=Integer.parseInt(DadosPont[PosicaoJogador]);
        pont=variavel.correrJogo2();
        System.out.println("PONTOS TOTAIS\n"+pont);
        DadosPont[PosicaoJogador]=String.valueOf(pont+pontAnt);
        Limpeza();
        SavePontuacaoN();
    }

public static void Sair(){
        System.out.println("Sair");

    }
    
public static String[] Utilizadores(){ 
    String[] utili= new String[100];
    int add=0;
    String path = Paths.get("").toAbsolutePath().toString();
    try {
            List<String> allLines = Files.readAllLines(Paths.get(path+"\\RegistoJogadores.txt"));
            for (String line : allLines) 
            {
               utili[add++]= line;
            }
		} 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
   return utili;
  }

public static String[] CarregarPont(){ 
    String[] pont= new String[100];
    int add=0;
    String path = Paths.get("").toAbsolutePath().toString();
    try {
            List<String> allLines = Files.readAllLines(Paths.get(path+"\\PontuacaoJogadores.txt"));
            for (String line : allLines) 
            {
               pont[add++]= line;
            }
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    DadosPont=pont;
   return pont;
  }

public static void VerificaUtilizadores(String Verifica){ 
String[] utili= new String[100];
int add=0,i=0;
int opcao, tamanho, ok=0;
utili=Utilizadores();
tamanho=utili.length;
         for(i=0;i<tamanho;i++)
                    {
                        if(Verifica.equals(utili[i]))
                        {
                             System.out.println("Utilizador encontrado\n");
                             PosicaoJogador=i;
                             ok=1;
                        }
                     }
                    if(ok!=1)
                    {
                    System.out.println("Nome de Utilizador nao encontrado\n");
                    System.out.println("Nome de Utilizador '"+Verifica+"' vai ser adicionado\n"); 
                    SaveUtiliz(Verifica);
                    utili=Utilizadores();
                    tamanho=utili.length;
                    }
        for(i=0;i<tamanho;i++)
        {
            if(Verifica.equals(utili[i]))
            {
                 PosicaoJogador=i;
            }
         }
  }

public static void SaveUtiliz(String AddUtili){ 
String path = Paths.get("").toAbsolutePath().toString();
String data = "This is BufferedWriter.";
try(FileWriter localizacao = new FileWriter(path+"\\RegistoJogadores.txt", true);
            BufferedWriter dados = new BufferedWriter(localizacao);
            PrintWriter out = new PrintWriter(dados)){
            out.println(AddUtili);
        }catch (IOException e) {
            e.printStackTrace();
            System.out.println("O caminho não está correto!");
        }
}

public static void SavePontuacaoN() throws FileNotFoundException, IOException{
String path = Paths.get("").toAbsolutePath().toString();
FileWriter fw = new FileWriter(path+"\\PontuacaoJogadores.txt");
String[] savepont= new String[100];
int i=0,add=0;
savepont=DadosPont;

System.out.println("Jogador com "+DadosPont[PosicaoJogador]+" pontos acumulados\n");
        for (i = 0; i < 100; i++) {
                fw.write(savepont[i]+"\n");
	}
 
	fw.close();
}

public static void Limpeza() throws IOException{ 
String path = Paths.get("").toAbsolutePath().toString();
        FileWriter fwOb = new FileWriter(path+"\\PontuacaoJogadores.txt", false); 
        PrintWriter pwOb = new PrintWriter(fwOb, false);
        pwOb.flush();
        pwOb.close();
        fwOb.close();
}

public static void main(String[] args) throws IOException {
Scanner ler = new Scanner(System.in);
Scanner entrada = new Scanner(System.in);
String sf=null,sf1=null,opcao;
int pont=0;
        do{
            menu();
            opcao = entrada.next();

            switch(opcao)
            {
                case "1":
                    System.out.println("Nome do Utilizador: ");
                    sf1 = ler.next();  
                    sf = sf1.toLowerCase(); 
                    VerificaUtilizadores(sf);
                    CarregarPont();
                    Jogo1();
                    break;

                case "2":
                    System.out.println("Nome do Utilizador:");
                    sf1 = ler.next();  
                    sf = sf1.toLowerCase(); 
                    VerificaUtilizadores(sf);
                    CarregarPont();
                    Jogo2();
                    break;

                case "0":
                    Sair();
                    break;
  
                default:
                    System.out.println("Opção inválida.");
            }
        } while(!"0".equals(opcao));
    }
}